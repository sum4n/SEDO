
/* # =================================================== :: */
/* #                                                        */
/* # Author       ::sum4n                                   */
/* # Source       ::https://gitlab.com/sum4n/SEDO           */
/* # Maintainer   ::to_suman@outlook.com                    */
/* # Project      ::Application launcher                    */
/* #                                                        */
/* # =================================================== :: */

configuration {
  modi:                       "drun";
    show-icons:                 false;
    display-drun:               "drun";
    display-run:                "run";
    display-filebrowser:        "file:";
    display-window:             "windows";
  drun-display-format:        "{name} [<span weight='light' size='small'><i>({generic})</i></span>]";
  window-format:              "{w} · {c} · {t}";
}

@import                          "themes/colors.rasi"

* {
  font:                          "CodeNewRoman Nerd Font Bold 14";
    border-colour:               var(BDR);
    handle-colour:               var(selected);
    background-colour:           var(background);
    foreground-colour:           var(foreground);
    alternate-background:        var(BDR);
    normal-background:           var(background);
    normal-foreground:           var(foreground);
    urgent-background:           var(urgent);
    urgent-foreground:           var(background);
    active-background:           var(active);
    active-foreground:           var(background);
    selected-normal-background:  var(selected);
    selected-normal-foreground:  var(background);
    selected-urgent-background:  var(active);
    selected-urgent-foreground:  var(background);
    selected-active-background:  var(urgent);
    selected-active-foreground:  var(background);
    alternate-normal-background: var(background);
    alternate-normal-foreground: var(foreground);
    alternate-urgent-background: var(urgent);
    alternate-urgent-foreground: var(background);
    alternate-active-background: var(active);
    alternate-active-foreground: var(background);
}

window {
    transparency:                "real";
    location:                    north;
    anchor:                      north;
    fullscreen:                  false;
    width:                       100%;

    enabled:                     true;
    margin:                      0px;
    padding:                     0px;
    border:                      2px solid;
    border-radius:               0px;
    border-color:                @border-colour;
    cursor:                      "default";
    background-color:            @background-colour;
}

mainbox {
    enabled:                     true;
    spacing:                     10px;
    margin:                      0px;
    padding:                     2px;
    border:                      0px solid;
    border-radius:               0px 0px 0px 0px;
    border-color:                @border-colour;
    background-color:            transparent;
    children:                    [ "inputbar", "listview" ];
    orientation:                 horizontal;
}

inputbar {
    enabled:                     true;
    spacing:                     0px;
    margin:                      0px;
    padding:                     0px;
    border:                      0px;
    border-radius:               0px;
    border-color:                @border-colour;
    background-color:            @background-colour;
    text-color:                  @foreground-colour;
    children:                    [ "prompt", "entry" ];
}

prompt {
    enabled:                     true;
    padding:                     5px 5px;
    border:                      0px;
    border-radius:               0;
    border-color:                @border-colour;
    background-color:            @BGA;
    text-color:                  @foreground;
}
textbox-prompt-colon {
    enabled:                     true;
    expand:                      false;
    str:                         "::";
    background-color:            inherit;
    text-color:                  inherit;
}
entry {
    enabled:                     true;
    expand:                      false;
    padding:                     5px 10px;
    width:                       10%;
    border-radius:               0px;
    background-color:            @background;
    text-color:                  @foreground;
    cursor:                      text;
    placeholder:                 "type to filter...";
    placeholder-color:           inherit;
    vertical-align:              0.5;
    horizontal-align:            0;
}

listview {
    enabled:                     true;
    lines:                       100;
    cycle:                       true;
    dynamic:                     true;
    scrollbar:                   false;
    layout:                      horizontal;
    reverse:                     false;
    fixed-height:                true;
    fixed-columns:               true;

    spacing:                     5px;
    margin:                      0px;
    padding:                     0px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @border-colour;
    background-color:            transparent;
    text-color:                  @foreground-colour;
    cursor:                      "default";
}

scrollbar {
    handle-width:                5px ;
    handle-color:                @handle-colour;
    border-radius:               0px;
    background-color:            @alternate-background;
}

element {
  font:                         @font;
    enabled:                     true;
    spacing:                     10px;
    margin:                      0px;
    padding:                     5px 10px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @border-colour;
    background-color:            transparent;
    text-color:                  @foreground-colour;
    cursor:                      pointer;
  vertical-align:              0.5;
  horizontal-align:            0.0;
}
element normal.normal {
  font:                         @font;
    background-color:            var(normal-background);
    text-color:                  var(normal-foreground);
}
element normal.urgent {
    background-color:            var(urgent-background);
    text-color:                  var(urgent-foreground);
}
element normal.active {
    background-color:            var(active-background);
    text-color:                  var(active-foreground);
}
element selected.normal {
    background-color:            @selected-normal-background;
    text-color:                  @selected-normal-foreground;
}
element selected.urgent {
    background-color:            var(selected-urgent-background);
    text-color:                  var(selected-urgent-foreground);
}
element selected.active {
    background-color:            var(selected-active-background);
    text-color:                  var(selected-active-foreground);
}
element alternate.normal {
    background-color:            var(alternate-normal-background);
    text-color:                  var(alternate-normal-foreground);
}
element alternate.urgent {
    background-color:            var(alternate-urgent-background);
    text-color:                  var(alternate-urgent-foreground);
}
element alternate.active {
    background-color:            var(alternate-active-background);
    text-color:                  var(alternate-active-foreground);
}
element-icon {
    background-color:            transparent;
    text-color:                  inherit;
    size:                        24px;
    cursor:                      inherit;
}
element-text {
  font:                         @font;
    background-color:            transparent;
    text-color:                  inherit;
    highlight:                   @off;
    cursor:                      inherit;
    vertical-align:              0.5;
    horizontal-align:            0.0;
}

mode-switcher{
    enabled:                     true;
    spacing:                     10px;
    margin:                      0px;
    padding:                     0px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @border-colour;
    background-color:            transparent;
    text-color:                  @foreground-colour;
}
button {
    padding:                     10px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @border-colour;
    background-color:            @alternate-background;
    text-color:                  inherit;
    cursor:                      pointer;
}
button selected {
    background-color:            var(selected-normal-background);
    text-color:                  var(selected-normal-foreground);
}

message {
    enabled:                     true;
    margin:                      0px;
    padding:                     0px;
    border:                      0px solid;
    border-radius:               0px 0px 0px 0px;
    border-color:                @border-colour;
    background-color:            transparent;
    text-color:                  @foreground-colour;
}
textbox {
    padding:                     5px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @border-colour;
    background-color:            @alternate-background;
    text-color:                  @foreground-colour;
    vertical-align:              0.5;
    horizontal-align:            0.0;
    highlight:                   none;
    placeholder-color:           @foreground-colour;
    blink:                       true;
    markup:                      true;
}
error-message {
    padding:                     0px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @border-colour;
    background-color:            @background-colour;
    text-color:                  @foreground-colour;
}
