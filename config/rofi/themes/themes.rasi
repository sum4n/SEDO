configuration {
    show-icons:                     false;
    display-drun: 					        "";
    drun-display-format:            "{icon} {name}";
    disable-history:                false;
	click-to-exit: 					          true;
}

@import "colors.rasi"

* {

  font:   "JetBrainsMono Nerd Font 14";

}

/**/

window {
    transparency:                   "real";
    background-color:               @background;
    text-color:                     @foreground;
    border:                  		    2px;
    border-color:                  	@BGA;
    border-radius:                  8px;
    width:                          500px;
    location:                       center;
    anchor:                         center;
    x-offset:                       0;
    y-offset:                       0;
}

/**/

entry {
    background-color:               @background;
    text-color:                     @background;
    placeholder-color:              @background;
    placeholder:                    "";
    expand:                         true;
    horizontal-align:               0;
    blink:                          false;
    padding:                        6px;
}

prompt {
    enabled: 					             true;
	margin: 						             5px 5px 5px 30px;
    padding:    		          		 0px;
	background-color:           		 @BG;
	text-color: 			          		 @FG;
  text-padding:   5px;
  font:                            "feather Bold 15";
    border:                  		   0px 0px 0px 0px;
    border-color:                  @BGA;
    border-radius:                 0px;
}


/**/

inputbar {
	children: 						            [ prompt ];
    spacing:                        0;
    background-color:               @BG;
    text-color:                     @foreground;
    border:                  		    0px 0px 6px 0px;
    border-radius:                  0px;
    border-color:                  	@BDR;
    expand:                         false;
    margin:                         0px 0px 0px 0px;
    padding:                        5px;
    position:                       center;
}

case-indicator {
    background-color:               @background;
    text-color:                     @foreground;
    spacing:                        0;
}


/**/

listview {
    background-color:               @background;
    columns:                        2;
    lines:							            5;
    spacing:                        4px;
    cycle:                          true;
    dynamic:                        true;
    layout:                         vertical;
}

mainbox {
    background-color:               @background;
    children:                       [ inputbar, listview ];
    spacing:                       	10px;
    padding:                        30px;
}

/**/

element {
    background-color:               @background;
    text-color:                     @foreground;
    orientation:                    horizontal;
    border-radius:                  0px;
    padding:                        5px;
}

element-icon {
    background-color: 				inherit;
    text-color:       				inherit;
    horizontal-align:               0.5;
    vertical-align:                 0.5;
    size:                           0px;
    border:                         0px;
}

element-text {
    background-color: 				inherit;
    text-color:       				inherit;
    expand:                         true;
    horizontal-align:               0;
    vertical-align:                 0.5;
    margin:                         2px 0px 2px 30px;
}

element selected {
    background-color:               @selected;
    text-color:                     @background;
    border:                  		    0px;
    border-radius:                  30px;
    border-color:                  	@selected;
    location:                       center;
}

element.active,
element.selected.urgent {
  background-color: @on;
  text-color: @background;
  border-color: @on;
}

element.selected.urgent {
  border-color: @selected;
}

element.urgent,
element.selected.active {
  background-color: @off;
  text-color: @background;
  border-color: @off;
}

element.selected.active {
  border-color: @selected;
}

/**/
