
# Author   -  sum4n
# Source   -  https://gitlab.com/sum4n/SEDO
# Maintainer -  to_suman@outlook.com

##---------- Keybindings for Termial and Termial-apps ----------##

# Terminal (alacritty)
super + Return
    myterm

super + shift + Return
	myterm -f

# Terminal Apps
super + o; {h,m}
	alacritty --class 'float,float' --config-file ~/.config/alacritty/alacritty.yml -e {htop,ncmpcpp}

##---------- Rofi Scripts && other bash script keys ----------##

# screenshot and asroot program
super + l; {r,s}
	sh ~/.config/rofi/bin/{asroot,screenshot}

super + l; b
  bookmarks

# edit all config-files
super + c
  edit-configs

# powermenu
super + x
  bash $HOME/.config/rofi/bin/powermenu

# window finder
super + w
  bash $HOME/.config/rofi/bin/windows

# theme selector
super + t
  sedo_selector

# rofi wifi and emoji menu
super + {n,e}
  {rofi-wifi-menu.sh,rofi -modi emoji -show emoji} 

# Rofi menu
super + space
	sh ~/.config/rofi/bin/launcher

super + shift + d
	rofi -show run 

# Color Picker Gpick
super + p
	color-gpick

# geometry of an window 
super + s; z
  description

## --- Scratch-pad --- ##

alt + s; Return
  scratch_i --term
alt + s; f
  scratch_i --lf
alt + s; m
  scratch_i --mus

## --- bin scripts --- ##

super + l; alt {s}
  {autoscript.sh}


##---------- Applications ----------##

# Launch Apps
super + shift + {f,b,e}
	{thunar,firefox,subl}


##---------- Brighness/volume/music Keys ----------##

# Brighness control
alt + ctrl + period
   backlit-mgr --inc
alt + ctrl + comma
   backlit-mgr --dec

#Raises volume
alt + equal
  volume-mgr --inc

#Lowers volume
alt + minus
	volume-mgr --dec

#Mute
alt + 0
	volume-mgr --toggle

# Music control
ctrl + m ; {n,p,t,s}
	mpc {next,prev,toggle,stop}

# MPV control
super + l; m
  mpv_launcher

ctrl + m; ctrl + n
  mpv_control --next
  
ctrl + m; ctrl + p
  mpv_control --prev

ctrl + m; ctrl + t
  mpv_control --pause

##---------- Bspwm ----------##

# Close App
super + BackSpace
  xdo close

super + shift + x
  xkill

#----#----#----#
# Reload Keybindings
super + Escape
	pkill -USR1 -x sxhkd


#----#----#----#
# Quit/Restart bspwm
ctrl + shift + r
	bspc wm -r

#----#----#----#
# Fullscreen or Monocle
super + f
    bspc node -t "~"fullscreen

# alternate between the tiled and monocle layout
alt + m
	bspc desktop -l next

# Toggle beetwen floating & tiled
super + slash
    bspc node -t "~"{floating,tiled}

# Pseudo Tiled & tiled modh
# super + shift + space
#     bspc node -t "~"{pseudo_tiled,tiled}


#----#----#----#
# FOCUS

# focus the node in the given direction
alt + {_,shift + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}

# Change focus to next window, including floating window
alt + {_,shift + }n
  bspc node -f {next.local,prev.local}

# Switch workspace
alt + {bracketleft,bracketright}
	bspc desktop -f {prev.local,next.local}

# Switch to last opened workspace
super + Tab
	bspc node -f last

# Switch to last opened desktop
super + grave
  bspc desktop -f last


#----#----#----#
# move and resize windows

# Send focused window to another workspace
super + {_,shift + }{1-8}
	bspc {desktop -f,node -d} '^{1-8}'

# Expanding windows
super + control + {Left,Right,Up,Down}
	bspc node -z {left -20 0,right 20 0,top 0 -20,bottom 0 20}
	
## Shrinking windows
super + ctrl + shift + {Left,Right,Up,Down}
	bspc node -z {left 20 0,right -20 0,top 0 20,bottom 0 -20}


#----#----#----#
	
# for hide and unhide windows
super + alt + i
  bspwinmask

#---- End Of File ----#

