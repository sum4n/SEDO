# Gotham colors

background='#0C1014'
foreground='#99d1ce'
cursor='#2AA889'

color0='#091f2e'
color8='#245361'

color1='#c23127'
color9='#c23127'

color2='#2aa889'
color10='#2aa889'

color3='#edb443'
color11='#edb443'

color4='#599cab'
color12='#599cab'

color5='#888ca6'
color13='#888ca6'

color6='#33859E'
color14='#33859E'

color7='#d3ebe9'
color15='#4E5166'
