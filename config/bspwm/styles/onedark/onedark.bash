# Author 	 -  sum4n
# Source 	 -  https://gitlab.com/sum4n/SEDO
# Maintainer -  to_suman@outlook.com

foreground='#bbc2cf'
background='#282c34'
cursor='#bbc2cf'

color0='#1c1f24'
color1='#ff6c6b'
color2='#98be65'
color3='#Ecbe7b'
color4='#51afef'
color5='#c678dd'
color6='#5699af'
color7='#ABB2BF'
color8='#5b6268'
color9='#da8548'
color10='#A2CD83'
color11='#EFCA84'
color12='#3071db'
color13='#a9a1e1'
color14='#46d9ff'
color15='#dfdfdf'
