# ----------------------------------\
#          TOKYO-NIGHT-DARK
# ----------------------------------\

# Special
background='#1a1b27'
foreground='#c0caf5'
cursor='#c0caf5'

# Colors
color0='#24283b'
color1='#f7768e'
color2='#9ece6a'
color3='#e0af68'
color4='#7aa2f7'
color5='#bb9af7'
color6='#7dcfff'
color7='#cfc9c2'
color8='#565a6e'
color9='#8c4351'
color10='#485e30'
color11='#ff9e64'
color12='#166775'
color13='#5a4a78'
color14='#2ac3de'
color15='#9699a3'
