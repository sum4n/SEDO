#!/bin/sh

# Author 	 -  sum4n
# Source 	 -  https://gitlab.com/sum4n/SEDO
# Maintainer -  to_suman@outlook.com
# Main Source - https://github.com/ChristianChiarulli \\ Christian Chiarulli

#----#----#----#
# source files 
source $ZDOTDIR/zsh-function
[[ -f ~/.zshrc-personal ]] && . ~/.zshrc-personal

#----#----#----#
# source zsh-specific files
zsh_add_file "zsh-prompt"
zsh_add_file "alias"
zsh_add_file "env_value"

#----#----#----#
# keybinds are works like emacs
# bindkey -e
# keybinds are works like emacs
bindkey -v

unsetopt BEEP

#----#----#----#
# some useful options (man zshoptions)
setopt autocd extendedglob nomatch menucomplete
setopt interactive_comments
stty stop undef		# Disable ctrl-s to freeze terminal.
zle_highlight=('paste:none')

#----#----#----#
# completions
autoload -Uz compinit
zstyle ':completion:*' menu select
# zstyle ':completion::complete:lsof:*' menu yes select
zmodload zsh/complist
compinit

autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

#----#----#----#
# Colors
autoload -Uz colors && colors

#----#----#----#
# Load plugins
zsh_add_plugin "zsh-users/zsh-autosuggestions"
zsh_add_plugin "zsh-users/zsh-syntax-highlighting"

# FZF
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

#----#----#----#
# History of shell
HISTFILE="$XDG_CONFIG_HOME"/zsh/history
HISTSIZE=1000000
SAVEHIST=1000000

