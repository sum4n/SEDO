--------------------------------------------------
-- Author           ::sum4n                     --
-- Source           ::https://gitlab.com/sum4n  --
-- Maintainer       ::to_sum4n@outlook.com      --
--------------------------------------------------

---- Import all libraries ----
pcall(require, "awful.hotkeys_popup.keys")
pcall(require, "luarocks.loader")
pcall(require, "awful.autofocus")
local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox") -- widget library
local beautiful = require("beautiful") -- theme handling library
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
---- import library end ----

---- Variable define ----
pcall(beautiful.init(require("config"))) -- Themes define colours, icons, font and wallpapers.

-- Custom variables
local terminal = "alacritty"
local editor = os.getenv("EDITOR") or "alacritty -e nvim"
local editor_cmd = terminal .. " -e " .. editor
local modkey = "Mod4"
local altkey = "Mod1"
---- variables end ----

---- Table of layouts ----
awful.layout.layouts = {
	awful.layout.suit.tile,
	awful.layout.suit.floating,
	awful.layout.suit.max,
}
---- layout end ----

---- Menu ----
-- Create a launcher widget and a main menu
local myawesomemenu = {
	{
		"hotkeys",
		function()
			hotkeys_popup.show_help(nil, awful.screen.focused())
		end,
	},
	{ "manual", terminal .. " -e man awesome" },
	{ "edit config", editor_cmd .. " " .. awesome.conffile },
	{ "restart", awesome.restart },
	{
		"quit",
		function()
			awesome.quit()
		end,
	},
}

local mymainmenu = awful.menu({
	items = {
		{ "awesome", myawesomemenu, beautiful.awesome_icon },
		{ "open terminal", terminal },
	},
})

local mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
---- menu end ----

-- Keyboard map indicator and switcher
local mykeyboardlayout = awful.widget.keyboardlayout()

---- Wibar ----
-- Create a textclock widget
local mytextclock = wibox.widget.textclock()

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
	awful.button({}, 1, function(t)
		t:view_only()
	end),
	awful.button({ modkey }, 1, function(t)
		if client.focus then
			client.focus:move_to_tag(t)
		end
	end),
	awful.button({}, 3, awful.tag.viewtoggle),
	awful.button({ modkey }, 3, function(t)
		if client.focus then
			client.focus:toggle_tag(t)
		end
	end),
	awful.button({}, 4, function(t)
		awful.tag.viewnext(t.screen)
	end),
	awful.button({}, 5, function(t)
		awful.tag.viewprev(t.screen)
	end)
)

local tasklist_buttons = gears.table.join(
	awful.button({}, 1, function(c)
		if c == client.focus then
			c.minimized = true
		else
			c:emit_signal("request::activate", "tasklist", { raise = true })
		end
	end),
	awful.button({}, 3, function()
		awful.menu.client_list({ theme = { width = 250 } })
	end),
	awful.button({}, 4, function()
		awful.client.focus.byidx(1)
	end),
	awful.button({}, 5, function()
		awful.client.focus.byidx(-1)
	end)
)

awful.screen.connect_for_each_screen(function(s)
	-- Each screen has its own tag table.
	awful.tag({ "1", "2", "3", "4", "5", "6" }, s, awful.layout.layouts[1])

	-- Create a promptbox for each screen
	s.mypromptbox = awful.widget.prompt()
	-- Create an imagebox widget which will contain an icon indicating which layout we're using.
	-- We need one layoutbox per screen.
	s.mylayoutbox = awful.widget.layoutbox(s)
	s.mylayoutbox:buttons(gears.table.join(
		awful.button({}, 1, function()
			awful.layout.inc(1)
		end),
		awful.button({}, 3, function()
			awful.layout.inc(-1)
		end),
		awful.button({}, 4, function()
			awful.layout.inc(1)
		end),
		awful.button({}, 5, function()
			awful.layout.inc(-1)
		end)
	))
	-- Create a taglist widget
	s.mytaglist = awful.widget.taglist({
		screen = s,
		filter = awful.widget.taglist.filter.all,
		buttons = taglist_buttons,
	})

	-- Create a tasklist widget
	s.mytasklist = awful.widget.tasklist({
		screen = s,
		filter = awful.widget.tasklist.filter.currenttags,
		buttons = tasklist_buttons,
	})

	-- Create the wibox
	s.mywibox = awful.wibar({ position = "top", screen = s })

	-- Add widgets to the wibox
	s.mywibox:setup({
		layout = wibox.layout.align.horizontal,
		{ -- Left widgets
			layout = wibox.layout.fixed.horizontal,
			-- mylauncher,
			s.mytaglist,
			s.mypromptbox,
		},
		s.mytasklist, -- Middle widget
		{ -- Right widgets
			layout = wibox.layout.fixed.horizontal,
			wibox.widget.systray(),
			mytextclock,
			s.mylayoutbox,
		},
	})
end)
---- wibar end ----

---- Mouse bindings ----
root.buttons(gears.table.join(
	awful.button({}, 3, function()
		mymainmenu:toggle()
	end),
	awful.button({}, 4, awful.tag.viewnext),
	awful.button({}, 5, awful.tag.viewprev)
))
---- mouse end ----

---- keybinds ----
local key = awful.key
local globalkeys = gears.table.join(
	key({ modkey, "Shift" }, "h", hotkeys_popup.show_help, { description = "show help", group = "awesome" }),
	key({ modkey }, "bracketleft", awful.tag.viewprev, { description = "view previous", group = "tag" }),
	key({ modkey }, "bracketright", awful.tag.viewnext, { description = "view next", group = "tag" }),
	key({ modkey }, "grave", awful.tag.history.restore, { description = "go back", group = "tag" }),

	key({ modkey, "Shift" }, "w", function() -- mouse left click menu
		mymainmenu:show()
	end, { description = "show main menu", group = "awesome" }),

	---- cycle through all windows ----
	key({ altkey }, "n", function() -- next
		awful.client.focus.byidx(1)
	end, { description = "focus next by index", group = "client" }),
	key({ altkey }, "p", function() -- previous
		awful.client.focus.byidx(-1)
	end, { description = "focus previous by index", group = "client" }),

	---- Layout manipulation ----
	key({ modkey, "Shift" }, "j", function()
		awful.client.swap.byidx(1)
	end, { description = "swap with next client by index", group = "client" }),
	key({ modkey, "Shift" }, "k", function()
		awful.client.swap.byidx(-1)
	end, { description = "swap with previous client by index", group = "client" }),

	---- jumpto urgent window ----
	key(
		{ modkey, altkey },
		"u",
		awful.client.urgent.jumpto,
		{ description = "jump to urgent client", group = "client" }
	),

	---- cycle through history in current desktop ----
	key({ modkey }, "Tab", function()
		awful.client.focus.history.previous()
		if client.focus then
			client.focus:raise()
		end
	end, { description = "go back", group = "client" }),

	---- Standard program ----
	-- key({ modkey }, "Return", function() -- terminal
	-- 	awful.spawn(terminal)
	-- end, { description = "open a terminal", group = "launcher" }),
	--
	-- key({ modkey, "Shift" }, "f", function() -- file-manager
	-- 	awful.spawn("pcmanfm")
	-- end, { description = "launch pcmanfm", group = "launcher" }),

	---- quit and restart ----
	key({ modkey, "Control" }, "r", awesome.restart, { description = "reload awesome", group = "awesome" }), -- reload awesome
	key({ modkey, "Shift" }, "q", awesome.quit, { description = "quit awesome", group = "awesome" }), -- quit awesome

	---- control and resize windows and layouts ----
	key({ modkey }, "Right", function() -- grow window size
		awful.tag.incmwfact(0.05)
	end, { description = "increase master width factor", group = "layout" }),

	key({ modkey }, "Left", function() -- decrease window size
		awful.tag.incmwfact(-0.05)
	end, { description = "decrease master width factor", group = "layout" }),

	key({ modkey, "Control" }, "h", function()
		awful.tag.incncol(1, nil, true)
	end, { description = "increase the number of columns", group = "layout" }),
	key({ modkey, "Control" }, "l", function()
		awful.tag.incncol(-1, nil, true)
	end, { description = "decrease the number of columns", group = "layout" }),

	key({ modkey, altkey, "Control" }, "space", function() -- cycle layouts
		awful.layout.inc(1)
	end, { description = "select next layout", group = "layout" }),

	---- run prompt rofi ----
	key({ modkey }, "space", function()
		awful.spawn("rofi -show drun")
	end, { description = "rofi launcher", group = "launcher" })
)

local clientkeys = gears.table.join(

	key({ modkey }, "f", function(c) -- toggle fullscreen
		c.fullscreen = not c.fullscreen
		c:raise()
	end, { description = "toggle fullscreen", group = "client" }),

	key({ modkey }, "BackSpace", function(c) -- kill focused window
		c:kill()
	end, { description = "close", group = "client" }),

	key({ modkey }, "slash", awful.client.floating.toggle, { description = "toggle floating", group = "client" }), -- toggle floating

	key({ altkey }, "m", function(c) -- maximize/unmaximize window
		c.maximized = not c.maximized
		c:raise()
	end, { description = "(un)maximize", group = "client" }),

	key({ modkey, altkey }, "i", function(c) -- minimize window
		c.minimized = true
	end, { description = "minimize", group = "client" }),

	key({ altkey, "Shift" }, "i", function() -- unminimize window
		local c = awful.client.restore()
		if c then
			c:emit_signal("request::activate", "key.unminimize", { raise = true })
		end
	end, { description = "restore minimized", group = "client" })
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
	globalkeys = gears.table.join(
		globalkeys,
		-- View tag only.
		key({ modkey }, "#" .. i + 9, function()
			local screen = awful.screen.focused()
			local tag = screen.tags[i]
			if tag then
				tag:view_only()
			end
		end, { description = "view tag #" .. i, group = "tag" }),
		-- Toggle tag display.
		key({ modkey, "Control" }, "#" .. i + 9, function()
			local screen = awful.screen.focused()
			local tag = screen.tags[i]
			if tag then
				awful.tag.viewtoggle(tag)
			end
		end, { description = "toggle tag #" .. i, group = "tag" }),
		-- Move client to tag.
		key({ modkey, "Shift" }, "#" .. i + 9, function()
			if client.focus then
				local tag = client.focus.screen.tags[i]
				if tag then
					client.focus:move_to_tag(tag)
				end
			end
		end, { description = "move focused client to tag #" .. i, group = "tag" }),
		-- Toggle tag on focused client.
		key({ modkey, "Control", "Shift" }, "#" .. i + 9, function()
			if client.focus then
				local tag = client.focus.screen.tags[i]
				if tag then
					client.focus:toggle_tag(tag)
				end
			end
		end, { description = "toggle focused client on tag #" .. i, group = "tag" })
	)
end

local clientbuttons = gears.table.join(
	awful.button({}, 1, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
	end),
	awful.button({ modkey }, 1, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
		awful.mouse.client.move(c)
	end),
	awful.button({ modkey }, 3, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
		awful.mouse.client.resize(c)
	end)
)

-- Set keys
root.keys(globalkeys)
---- keybind end ----

---- Rules ----
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
	-- All clients will match this rule.
	{
		rule = {},
		properties = {
			border_width = beautiful.border_width,
			border_color = beautiful.border_normal,
			focus = awful.client.focus.filter,
			raise = true,
			keys = clientkeys,
			buttons = clientbuttons,
			screen = awful.screen.preferred,
			placement = awful.placement.no_overlap + awful.placement.no_offscreen,
		},
	},

	-- Floating clients.
	{
		rule_any = {
			instance = {
				"DTA", -- Firefox addon DownThemAll.
				"copyq", -- Includes session name in class.
				"pinentry",
			},
			class = {
				"Arandr",
				"Pcmanfm",
				"Blueman-manager",
				"Gpick",
				"Kruler",
				"MessageWin", -- kalarm.
				"Sxiv",
				"Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
				"Wpa_gui",
				"veromix",
				"xtightvncviewer",
				"Viewnior",
				"File-roller",
				"Gnome-disks",
				"Pavucontrol",
				"float",
			},

			-- Note that the name property shown in xprop might be set slightly after creation of the client
			-- and the name shown there might not match defined rules here.
			name = {
				"Event Tester", -- xev.
			},
			role = {
				"AlarmWindow", -- Thunderbird's calendar.
				"ConfigManager", -- Thunderbird's about:config.
				"pop-up", -- e.g. Google Chrome's (detached) Developer Tools.
			},
		},
		properties = { floating = true },
	},

	-- Set applications to launch different desktops
	{ rule = { class = "firefox" }, properties = { screen = 1, tag = "2" } },
	{ rule = { class = "Pcmanfm" }, properties = { screen = 1, tag = "3" } },
	-- tag-5
	{ rule = { class = "Nitrogen" }, properties = { floating = true, screen = 1, tag = "5" } },
	{ rule = { class = "Xfce4-settings-manager" }, properties = { floating = true, screen = 1, tag = "5" } },
	{ rule = { class = "Gpick" }, properties = { floating = true, screen = 1, tag = "5" } },
	{ rule = { class = "Lxappearance" }, properties = { floating = true, screen = 1, tag = "5" } },
	{ rule = { class = "qt5ct" }, properties = { floating = true, screen = 1, tag = "5" } },
	{ rule = { class = "Gparted" }, properties = { screen = 1, tag = "5" } },
	{ rule = { class = "Virt-manager" }, properties = { screen = 1, tag = "5" } },
	{ rule = { class = "Gnome-boxes" }, properties = { screen = 1, tag = "5" } },
	-- tag-6
	{ rule = { class = "vlc" }, properties = { floating = true, screen = 1, tag = "6" } },
	{ rule = { class = "Spotify" }, properties = { floating = true, screen = 1, tag = "6" } },
	{ rule = { class = "mpv" }, properties = { floating = true, screen = 1, tag = "6" } },

	-- scratchpad
	{ rule = { class = "sc_term" }, properties = { screen = 1, floating = true, sticky = true, ontop = true } },
	{ rule = { class = "sc_lf" }, properties = { screen = 1, floating = true, sticky = true, ontop = true } },
	{ rule = { class = "sc_mus" }, properties = { screen = 1, floating = true, sticky = true, ontop = true } },
}
---- rule end ----

---- Signals ----

-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
	-- Set the windows at the slave,
	-- i.e. put it at the end of others instead of setting it master.
	-- if not awesome.startup then awful.client.setslave(c) end

	if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
		-- Prevent clients from being unreachable after screen count changes.
		awful.placement.no_offscreen(c)
	end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
	c:emit_signal("request::activate", "mouse_enter", { raise = false })
end)

client.connect_signal("focus", function(c)
	c.border_color = beautiful.border_focus
end)
client.connect_signal("unfocus", function(c)
	c.border_color = beautiful.border_normal
end)
---- signal end ----

---- custom config ----
-- Autostart applications
pcall(require("config.autostart"))

---- custom end ----
