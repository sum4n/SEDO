---- Autostart applications ----
-- import library --
local awful = require("awful")
local sh = awful.spawn.with_shell

-- list all applications --
sh("$HOME/.config/awesome/config/auto_start.sh")
sh("dunst")
