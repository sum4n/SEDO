# SEDO-BSPWM

## ⚠️ All of the color palette looks may changed accordingly but images are not updated so Please take it with bit of salt ⚠️

## GruvBox-Material
![](assets/glance/gruvbox.png)
## EverForest
![](assets/glance/ever_forest.png)
## Gotham
![](assets/glance/gotham.png)
## Dracula
![](assets/glance/dracula.png)
## OneDark
![](assets/glance/one_dark.png)
## TokyoNight
![](assets/glance/tokyo_night.png)

### Rofi
![](assets/glance/rofi.png)
### PowerMenu
![](assets/glance/power_menu.png)
### Themes Available
![](assets/glance/themes.png)


### 💠 Installation:

This installer can download and setup BSPWM window manager in vanilla **[DEBIAN-Testing]** or **[ARCH]** based machines.
No need any desktop environment for setup. It downloads all it's needed packages and setup.

## Install the rice via installer
- **First download the installer**
```sh
wget https://gitlab.com/sum4n/SEDO/-/raw/main/sedo_installer
```
- **Now give it execute permissions**
```sh
chmod +x sedo_installer
```
- **Finally run the installer**
```sh
./sedo_installer
```

## Or clone my rice

* Install the dependencies first
### DEBIAN
```
sudo apt install mpv zsh hwinfo htop feh picom lightdm brightnessctl xautolock cron light bspwm file-roller mlocate pulsemixer rofi suckless-tools network-manager-gnome dunst policykit-1-gnome sxhkd nemo pcmanfm lxappearance qt5ct udiskie copyq fish nitrogen vim viewnior volumeicon-alsa ranger python3-pip npm mpd ncmpcpp mpc gparted pavucontrol kdeconnect ufw xclip maim git curl wget ripgrep polybar htop neofetch playerctl pcmanfm acpi gpick qt5-style-kvantum numlockx xfce4-settings i3lock-color xfce4-power-manager slop network-manager xorg xinit pulseaudio build-essential bluez blueman xdotool xdo autoconf exa lightdm-gtk-greeter lightdm-gtk-greeter-settings firefox-esr lf fd-find fzf alacritty cava socat jq kitty
```
### ARCH
```
sudo pacman -Sy lightdm fzf fd feh zsh htop hwinfo mpv kvantum numlockx brightnessctl light xautolock file-roller mlocate rofi-emoji pulsemixer bspwm rofi dmenu network-manager-applet dunst polkit-gnome sxhkd alacritty nemo lxappearance qt5ct udiskie copyq fish nitrogen vim neovim viewnior volumeicon pacman-contrib ranger python-pip npm mpd ncmpcpp mpc gparted yarn pavucontrol kdeconnect ufw gufw xclip maim git curl wget lazygit python-pywal ripgrep polybar ntfs-3g playerctl btop neofetch android-tools android-file-transfer pcmanfm acpi cronie libxcrypt-compat gpick fortune-mod xfce4-settings xfce4-power-manager networkmanager archlinux-keyring xorg xorg-xinit pulseaudio pulseaudio-alsa xf86-video-amdgpu base-devel bluez bluez-utils blueman xdotool xdo autoconf slop exa firefox lightdm-gtk-greeter lightdm-gtk-greeter-settings papirus-icon-theme lf socat jq kitty
```
* Clone the actual repo
```
git clone https://gitlab.com/sum4n/SEDO
```
* **Clone the scripts repo it's like a dependenci for work properly**
```
git clone https://gitlab.com/sum4n/bin
```
* clone the fonts repo it's (optional) if you want you can change your font
```
git clone https://gitlab.com/sum4n/sedo-fonts
```

* **Suman Kumar Das**
* **https://www.gitlab.com/sum4n -- GitLab**
* **https://www.github.com/Suman-1410 -- GitHub**



