# ARCH INSTALLATION INSTRUCTION. It's for GPT and EFI system.

## For First installation

* Edit the `/etc/pacman.conf` file comment-out the `ParallelDownloads` option.
* Edit the `/etc/pacman.d/mirrorlist` choose your Country mirrors.

## Choose your Country mirrors

```bash
pacman -S reflector
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
reflector --protocol https --verbose --country '<your-country-name>' -l 8 --sort rate --save /etc/pacman.d/mirrorlist
```

### First set your key-maps

```bash
ls /usr/share/kbd/keymaps/**/*.map.gz | less <for list the available kemap>
loadkeys <your keymap>
```

### System clock

```bash
timedatectl set-ntp true
timedatectl status
```

### if you are using wireless connection then follow the commands.

```bash
iwctl
device list
station <device> scan
station <device> get-networks
station <device> connect <your_SSID>
```

### PARTITION YOUR DISK USING [CFDISK] IT'S MORE EASIER THAN THE OTHERS --- it's mandatory.

```bash
fdisk -l
cfdisk /path/to/directory
```

### Make your file-system.

* **For efi part:**
  
  ```bash
  mkfs.fat -F32 /path/to/EFI
  ```

* **For swap:**
  
  ```bash
  mkswap /path/to/swap
  swapon /path/to/swap
  ```

* **For root:**
  
  ```bash
  mkfs.ext4 /path/to/root
  ```

### If you created home partition then.

```bash
mkfs.ext4 /path/to/home
```

### Mount your desired partition.

```bash
mount /path/to/root /mnt
mkdir /mnt/home
mount /path/to/home /mnt/home
```

### Install system

```bash
pacstrap -i /mnt base linux linux-firmware sudo vim
```

### Generate fstab file

```bash
genfstab -U /mnt >> /mnt/etc/fstab
```

### CH-ROOT into system

```bash
arch-chroot /mnt
```

### Set TimeZone

```bash
ln -sf /usr/share/zoneinfo/YourRegion/YourCity /etc/localtime
hwclock --systohc --utc
```

### Generate your locale.

```bash
vim /etc/locale.gen <uncomment your locale>
locale-gen
```

### Edit your Host-Name and Hosts.

```bash
touch /etc/hostname
echo <yourHostName> > /etc/hostname
vim /etc/hosts
```

### ⚠ Add this lines to your hosts file

```
127.0.0.1   localhost
::1         localhost
127.0.1.1   <hostname>.localdomain  <hostname>
```

### Create user and password

```bash
passwd
useradd -m <username>
passwd <username>
usermod -aG wheel,audio,video,optical,storage <username>
```

### Edit sudoers file

```bash
EDITOR=vim visudo <uncomment wheel related line>
```

### Install Grub

```bash
pacman -S grub efibootmgr dosfstools os-prober mtools
```

### EFI mount

```bash
mkdir /boot/EFI
mount /dev/(efi.part) /boot/EFI
```

### Final grub install

```bash
grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck
grub-mkconfig -o /boot/grub/grub.cfg
```

### Install your Needed software.

* This provided applications are impotent

```bash
pacman -S networkmanager git curl wget bluez bluez-utils blueman
```

### Enable networking

```bash
systemctl enable NetworkManager.service
```

### Exit from ch-root

```bash
exit
umount -l /mnt
```

<!-- ecUQ2c2G-GyZmKuAu5 -->
