# 💠 Oracle-VirtualBox installation instruction (ARCH)

* **Install needed dependencies**
  
  ```bash
  sudo pacman -S virtualbox virtualbox-guest-iso virtualbox-guest-iso virtualbox-host-modules-arch virtualbox-guest-utils virtualbox-sdk
  ```

* **Add user to (vboxusers) group**
  
  ```bash
  sudo usermod -aG vboxusers $USER
  ```

* Enable the web service
  
  ```bash
  sudo systemctl enable vboxweb.service --now
  ```
  
  ### Download the extpack and Load it from file-manager. Do the following
  
  ```bash
  cd $HOME/Downloads
  curl -O https://download.virtualbox.org/virtualbox/7.0.6/Oracle_VM_VirtualBox_Extension_Pack-7.0.6a-155176.vbox-extpack
  chmod +x Oracle*
  ```
  
  ### ⚠ Next right-click on the downloaded file run with Oracle-virtual-box

# 💠 Virt-Manager installation

#### Checking Hardware virtualization Support and install dependencies

```bash
LC_ALL=C lscpu | grep Virtualization
sudo pacman -S qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat
sudo pacman -S iptables
sudo pacman -S libguestfs
```

* **Start KVM libvirt service**
  
  ```bash
  sudo systemctl enable libvirtd.service --now
  systemctl status libvirtd.service
  ```

* **Enable normal user account to use KVM**
  
  ```bash
  sudo $EDITOR /etc/libvirt/libvirtd.conf
  ```

* Set the UNIX domain socket group ownership to libvirt, (around line 85)
  
  ```bash
  unix_sock_group = "libvirt"
  ```

* Set the UNIX socket permissions for the R/W socket (around line 102)
  
  ```bash
  unix_sock_rw_perms = "0770"
  ```

* **Add your user account to libvirt group.**
  
  ```bash
  sudo usermod -a -G libvirt $USER
  newgrp libvirt
  sudo systemctl restart libvirtd.service
  ```

# 😀Now you are ready to use one of the virtualization 😀
